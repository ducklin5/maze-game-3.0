extends Sprite

var prev_pos
var prev_rot

func _ready():
	get_material().set_shader_param("texSizeLength", texture.get_size().length())
	get_material().set_shader_param("_NoiseHeight", 64)
	get_material().set_shader_param("_NoiseScale", 0.5)
	prev_pos = global_position
	prev_rot = rotation
	set_process(true)

func _process(delta):
	var current_pos = global_position
	var current_rot = rotation
	
	get_material().set_shader_param("prev_pos", prev_pos)
	get_material().set_shader_param("current_pos", current_pos)
	get_material().set_shader_param("prev_rot", prev_rot)
	get_material().set_shader_param("current_rot", current_rot)
	
	prev_pos = lerp(prev_pos, current_pos,clamp(delta*2,0,1))
	
	prev_rot = current_rot
	
	global_position = get_global_mouse_position()
	if Input.is_key_pressed(KEY_R):
		rotation += delta * 2*PI