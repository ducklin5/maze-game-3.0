extends Node
export var MAX_ENERGY= 100
sync var value = MAX_ENERGY
var efficiency = 1

signal energyChanged(current,maximum)

func changeBy(x):
	value += x / efficiency
	if value > MAX_ENERGY:
		value = MAX_ENERGY
	emit_signal("energyChanged", value, MAX_ENERGY)
