extends Node

var health_funcref = funcref( self, "health")
var energy_funcref = funcref( self, "energy")
var ammo_funcref = funcref( self, "ammo")

var items = [ {		"id" : 0,
					"name":"Health",
					"path":"res://Spritesheets/Halo.png",
					"color": Color(1,0,0,1),
					"rarity": 0.15,
					"group": 0,
					"amount": 20,
					"dropEffect": health_funcref
				},
				{	"id": 1,
					"name":"Energy",
					"path":"res://Spritesheets/Halo.png",
					"color": Color(0,0,1,1),
					"rarity": 0.25,
					"group": 0,
					"amount": 20,
					"dropEffect": energy_funcref
				},
				{	"id" : 2,
					"name":"Ammo",
					"path":"res://Spritesheets/Halo.png",
					"color": Color(1,0,1,1),
					"rarity": 0.2,
					"group": 0,
					"amount": 30,
					"dropEffect": ammo_funcref
				}
			]
var enemies = [{
				"name":"Bot",
				"dropRate":0.3,
				"rarity":0.3,
				"groups":[0,7]
			}]


func health(player):
	if player.health.value < player.health.MAX_HEALTH:
		player.health.heal(items[0].amount)
		return true
	else:
		return false
func energy(player):
	if player.energy.value < player.energy.MAX_ENERGY:
		player.energy.changeBy(items[1].amount)
		return true
	else:
		return false

func ammo(player):
	for weapon in player.weapons:
		if weapon.ammo < weapon.MAX_AMMO:
			weapon.ammoChangeBy(items[2].amount)
			return true
			break
		
	return false