extends "res://Script/Character.gd"
enum { ROAM = 0, WAIT = 1, CHASE = 2 }
var ray
var r = 600
var directionAngle = 0
var fovAngle = 2*PI
#var points={"far" : [] , "near" : []}
var objects = []
var state = ROAM
var globalDestination = Vector2(0,0)
var path = []
var checkpoints; var randCheckpoint; var newPath = []
var t
var bossWeight = 5

export var droptableID = 0

func _ready():
	t = Timer.new()
	ray = get_node("ThinkRay")
	ray.cast_to = Vector2(r,0)
	ray.set_collision_mask(get_collision_mask())
	health.MAX_HEALTH = 120; health.value = 120;
	checkpoints = get_parent().visited
	t.wait_time = 1
	t.one_shot = true
	t.start()
	add_child(t)
	


func think(delta):

	if state == ROAM and path.size() == 0 : #set a new destination if the current destination is already reached or is unreachable
		MOTION_SPEED = 200 + (randi() % 50) 
		randCheckpoint = checkpoints[rand_range(0,checkpoints.size())] + Vector2(0.5,0.5)
		globalDestination = randCheckpoint * get_parent().get_cell_size() * 3 + get_parent().global_position


	if state == CHASE:
		MOTION_SPEED = 400
		var prey 
		var distances = []
		for i in range(objects.size()):
			#distances[i] = (objects[i].global_position - global_position).length()
			prey = objects[i]
		var wr = weakref(prey);
		if wr.get_ref().is_inside_tree():
			lookDir = (wr.get_ref().global_position - global_position).normalized()
			globalDestination = wr.get_ref().global_position-lookDir*240

			rpc("shoot",lookDir)
	if t.is_stopped():
		newPath = gamestate.world.get_simple_path(global_position, globalDestination, false)
		t.start()
	else:
		scan()
		
	if newPath.size() < path.size() || path.size() == 0:
		path = newPath

	if path.size() > 0:
		var distance = path[0] - global_position
		if distance.length() > 64 :
			moveDir = distance.normalized() # direction of movement
			directionAngle = atan2(moveDir.x,moveDir.y) - PI/2
		else:
			path.remove(0)
#
	var stuck =  ( get_slide_count() > 0) 
	if state == ROAM and stuck:
		path = []
		moveDir = -2*moveDir


func scan():
	objects = []
	var alpha = directionAngle - fovAngle/2
	ray.set_rotation(-alpha)
	while alpha < directionAngle + fovAngle/2  :
		if ray.is_colliding():
			var body = ray.get_collider()
			if body and body.is_in_group("player") and !objects.has(body):
				state = CHASE
				objects.append(body)
		alpha+=0.5
		ray.set_rotation(-alpha)
		ray.force_raycast_update()
	if objects.size() == 0:
		state = ROAM
#
#func _draw():
#	# if there are points to draw
#	if path.size() > 1:
#		for p in path:
#			draw_circle(p - global_position, 3, Color(1, 0, 0)) # we draw a circle (convert to global position first)
#	draw_circle_arc_poly( Vector2(0,0), r, directionAngle - fovAngle/2, directionAngle + fovAngle/2, Color(1,1,0,0.1))
##	for p in points.far:
##		draw_line(Vector2(0,0), p, Color(1,0,0))
##	for p in points.near:
##		draw_line(Vector2(0,0), p, Color(0,0,1))
#	for p in objects:
#		draw_line(Vector2(0,0), get_global_transform().xform_inv(p.global_position), Color(0,1,0))

func draw_circle_arc_poly( center, radius, angle_from, angle_to, color ):
	var nb_points = 32
	var points_arc = []
	points_arc.push_back(center)
	var colors = PoolColorArray([color])

	for i in range(nb_points+1):
		var angle_point = angle_from + i*(angle_to-angle_from)/nb_points
		points_arc.push_back(center + Vector2( cos( angle_point ), -sin( angle_point) ) * radius)
	draw_polygon(points_arc, colors)

sync func dropItem(randSeed):
	.dropItem(randSeed)
	seed(randSeed)
	var thisEnemy = droptable.enemies[droptableID]
	var diceRoll = randf()
	if diceRoll < thisEnemy.dropRate:
		var itemDrop
		var loot = []
		var lootChance = []
		var invRaritySum = 0
		for item in droptable.items:
			if thisEnemy.groups.has(item.group) and thisEnemy.rarity >= item.rarity: #if the enemy can drop this item
				loot.append(item) #add it to the loot box
				invRaritySum += thisEnemy.rarity-item.rarity #add its inverse rarity to the total inverse rarity variable
		var prev = 0
		for item in loot:
			var current = (thisEnemy.rarity-item.rarity)/invRaritySum
			lootChance.append(prev+current)
			prev += current
		diceRoll = randf()
		var i=0
		while itemDrop == null:
			if diceRoll < lootChance[i]:
				itemDrop = loot[i]
			else: i+=1
		var dropScene = load("res://Scenes/Drops/genericDrop.tscn")
		var dropNode = dropScene.instance()
		dropNode.itemDrop = itemDrop
		dropNode.position = position
		get_parent().add_child(dropNode)
		hasDropped = true

func killed(shooter):
	if get_tree().is_network_server() and .killed(shooter):
		gamestate.rpc("bossCountDown",bossWeight)
		t.stop(); t.start(); yield(t, "timeout")
		queue_free()
