extends Node

# Default game port
var PORT = 8523

# Max number of players
const MAX_PEERS = 12

# Name for my player
var player_info = {"name": "PlayerX", "color": Color(randf(),randf(),randf())}
var localpid

# info for players in id:info_dict format
var players = {} 
enum {ALIVE = 1, DEAD = 2, WAITING = 3}
master var playerState = ALIVE

# world
var world
var roomsPerLayer = 3
var enemiesPerLayer = 6

#signals
signal player_list_changed()

#globals
var HUD
var spawnpoints
var killCounter = 0
var killsRequired = 100

#function to host game
func host_game(playerInfo, maxPeers = MAX_PEERS, port = 8523):
	#set the host player info
	player_info = playerInfo
	#new networking variable called host
	var host = NetworkedMultiplayerENet.new()
	#make it the server with a maximum of maxPeers connections
	host.create_server(port, maxPeers)
	#add the networking variable to the tree
	get_tree().set_network_peer(host)
	#add myself (server) to 
	players[1] = player_info

#join an existing game
func join_game(playerInfo, ip, port=8523):
	#set the client player name
	player_info = playerInfo
	#new networking variable called client
	var client = NetworkedMultiplayerENet.new()
	#make it a client on the server 'ip'
	client.create_client(ip, port)
	#add the networking variable to the tree
	get_tree().set_network_peer(client)

func _ready():
	get_tree().connect("connected_to_server", self, "_connected_ok") # run _connect_ok on client when it connects to server
	
func _connected_ok():
	# Im a Client
	# register my ID and info to all other peers including myself
	rpc("register_player", get_tree().get_network_unique_id(), player_info)
	#emit_signal("connection_succeeded")
	
sync func register_player(id, player_info):
	#print("peer " + str(get_tree().get_network_unique_id()) + " just ran register_player( " + str(id) + " , " + str(player_info) +" )")
	
	# If I am the server, let the new guy know about existing players
	if (get_tree().is_network_server()): 
		for p_id in players:
			#resgister remote players to new dude's player dict
			rpc_id(id, "register_player", p_id, players[p_id]) 
			#run register_player on remote player to add new dude to remote players' (except server's) player dict
			if p_id != 1:
				rpc_id(p_id, "register_player", id, player_info) 
	
	# Store the info
	players[id] = player_info
	
	emit_signal("player_list_changed")


func begin_game():
	#only the server can run this function
	assert(get_tree().is_network_server())
	### initiate world with a single random seed on all peers including server ###
	randomize() # randomize the server
	var s = randi() # create a seed, this variable will be used to a similar generate world on all peers
	add_world(s, players.size() +1 , roomsPerLayer) #generate the world on server and store the spawnpoints information
	rpc("add_world",s, players.size()+1 , roomsPerLayer) #generate the world on all peers

	#print(spawnpoints) #print spawnpoints for debugging
	
	# Create a dictionary with peer id and respective spawn points
	# in the form peerID:spawnPointID
	var peer2spawnPoint = {}
	var spawnIndex = 0
	for p in players:
		peer2spawnPoint[p] = spawnpoints[spawnIndex]
		spawnIndex += roomsPerLayer

	#print("players:" + str(players)) #print players for debugging
	#Call to pre-start game on all peers with the spawn points 
	rpc("pre_start_game", peer2spawnPoint)
	
sync func pre_start_game(peer2spawnPoint):
	#hide the lobby
	get_tree().get_root().get_node("lobby").hide()
	#HUD
	HUD = load("res://Scenes/HUD.tscn").instance()
	HUD.connect("respawn",self, "respawn")
	get_parent().add_child(HUD)
	var player_scene = load("res://Scenes/Player.tscn")
	
	#get the world node
	var world = get_tree().get_root().get_node("World")
	#hideMaps()
	
	#spawn the players
	for p_id in peer2spawnPoint:
		var spawn = peer2spawnPoint[p_id]
		var player = player_scene.instance()
		players[p_id].body = player
		
		
		player.p_id = p_id
		player.set_name(str(p_id)) # Use unique ID as node name
		player.set_character_name(players[p_id].name)
		player.set_character_color(players[p_id].color)
		player.set_network_master(p_id)
		spawn(p_id,spawn)
		
		
		
		#world.get_node(str(spawn.layerIndex)).set_maze_color(players[p_id].color)
		if player.is_network_master():
			localpid = p_id
			print(str(p_id)+ " is the master")
			for i in range (world.layers):
				HUD.mapState.append(HUD.UNKOWN)
			HUD.mapState[spawn.layerIndex] = HUD.ACTIVE
			HUD.updateMapIndicator()
			player.connect("mapChanged",HUD,"_on_mapChanged")
			player.get_node("Energy").connect("energyChanged",HUD,"_on_energyChanged")
			player.get_node("Health").connect("healthChanged",HUD,"_on_healthChanged")
			player.get_node("Health").connect("dead", HUD,"_player_died")
			player.get_node("Health").connect("dead", self, "cameraChange")
			
			player.get_node("camera").make_current()
			#player.get_parent().visible = true

	
	#spawn the Bots
	var bot = load("res://Scenes/Bot.tscn")
	var x = 0

	for spawn in spawnpoints:
		var free = true
		for p_id in peer2spawnPoint:
			free = spawn.path != peer2spawnPoint[p_id].path and free
		if free:
			for c in range(enemiesPerLayer/roomsPerLayer):
				enemySpawn (bot,
					"bot"+str(x), 
					spawn.layerIndex, 
					spawn.position+Vector2((c-3/1.5)*30,(c-3/1.5)*30)
				)
				x+=1
	
# add a randomly generated 3d maze and also return the its SpawnPoints Array
remote func add_world(worldSeed, layers, roomPerLayer):
	#new world instance
	world = load("res://Scenes/World.tscn").instance()
	world.randomSeed = worldSeed
	world.roomsValue = roomPerLayer
	world.layers = layers
	get_tree().get_root().add_child(world)
	
	#return array of spawn point dictionaries in the format {"layerIndex": int, "spawnPos": vector2d}
	spawnpoints = world.SpawnPoints

func hideMaps():
	for l in world.get_children():
		l.set_visible(false)

func cameraChange(newPlayer):
	newPlayer.get_node("camera").make_current()

master func respawn():
	#hideMaps()
	HUD.respawnButton.visible = false
	HUD.deathNotice.visible = false
	var selected = false

	var player = players[localpid].body
	while !selected:
		var spawn = spawnpoints[randi()%spawnpoints.size()]
		var spawnNode = get_node(spawn.path)
		if spawnNode.get_node("region").get_overlapping_bodies().size() == 0:
			#add player to scene
			
			rpc("spawn", localpid, spawn)
			for w in player.weapons:
				if w.ammo < w.MAX_AMMO/2:
					w.rset("ammo", int(w.MAX_AMMO/2))
		
			#world.get_child(spawn.layerIndex).set_visible(true)
			player.get_node("camera").make_current()
			player.emit_signal("mapChanged", spawn.layerIndex)
			
			selected = true
	playerState = ALIVE

sync func spawn(p_id,spawn):
	print("spawn")
	var player = players[p_id].body
	player.position = spawn.position
	var map = world.get_child(spawn.layerIndex)
	#player.set_collision_mask(map.get_collision_mask())
	#player.set_collision_layer(map.get_collision_layer())
	player.get_node("Health").heal(player.get_node("Health").MAX_HEALTH/player.get_node("Health").healMultiplier)
	map.add_child(player)

func enemySpawn (enemyType, enemyName, floorID, position):
	var enemy = enemyType.instance()
	enemy.name = enemyName
	enemy.set_position(position)
	var map = world.get_child(floorID)
	map.add_child(enemy)
	enemy.set_network_master(1)
	

# progression function and bot respawn
sync func bossCountDown(amount):
	killCounter += amount
	HUD.updateBossProg(killCounter,killsRequired)
	
	if killCounter == killsRequired:
		print("Boss Time!!!!!!!")
	else:
		var availabeMap 
		for map in world.get_children():
			var noPlayerPresent = true
			var enemyCount = 0
			for mapChild in map.get_children():
				if mapChild.is_in_group("player"):
					noPlayerPresent = false
				elif mapChild.is_in_group("bot"):
					enemyCount += 1
			if noPlayerPresent and enemyCount<enemiesPerLayer :
				availabeMap = map
				break
		if availabeMap:
			var layerIndex = availabeMap.get_index()
			var enemy = load("res://Scenes/Bot.tscn")
			for spawnNode in availabeMap.SpawnPoints.get_children():
				if spawnNode.get_node("region").get_overlapping_bodies().size() == 0:
					enemySpawn (enemy, "bot"+str(randi()), layerIndex, spawnNode.position)
		else:
			print("no free maps")
				
