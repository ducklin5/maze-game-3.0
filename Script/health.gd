extends Node
var MAX_HEALTH = 100
sync var value= MAX_HEALTH
var hitMultiplier = 1
var healMultiplier = 1

signal healthChanged(current,maximum)
signal dead(shooter)

sync func hit(x, shooter):
	var shooterName = shooter.name
	var finalHit = x * hitMultiplier
	if value< finalHit:
		finalHit = value
	
	value= value- finalHit
	
	emit_signal("healthChanged", value, MAX_HEALTH)
	
	if value <= 0:
		emit_signal("dead", shooter)
		print("killed by: " + shooterName)
	else:
		print("hit by: " + shooterName)
		var hL = hitLabel.new()
		hL.finalHit = -finalHit
		add_child(hL)
		
		get_node("healthBar").max_value = MAX_HEALTH
		get_node("healthBar").value = value
		get_node("healthBar").visible = true
		get_node("Timer").start()
		yield(get_node("Timer"), "timeout")
		get_node("healthBar").visible = false
		
	

sync func heal(x):
	value+= x * healMultiplier
	if value > MAX_HEALTH:
		value = MAX_HEALTH
	emit_signal("healthChanged", value, MAX_HEALTH)
	

class hitLabel:
	extends Label
	var finalHit = 0
	var lifeSpan = 2
	var time = 0
	func _ready():
		modulate = Color(1,0,0)
		rect_scale = Vector2(3,3)
		rect_position = Vector2(0,-30)
		set_text(str(finalHit))
		set_physics_process(true)
	func _physics_process(delta):
		time += delta
		if time > lifeSpan:
			queue_free()
