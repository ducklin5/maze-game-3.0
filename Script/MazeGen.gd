extends TileMap

var randomSeed = 9
var cols = 15
var rows = 15

enum {N=0,E=1,S=2,W=3}

onready var SpawnPoints = get_node("SpawnPoints")

var roomsValue = 1
var rooms = []
var roomCells = []
var roomSize = Vector2(3 , 3 )

var grid = []
var current = Vector2(0,0)
var visited = []

var portals = {}

signal mazePercentDone(x)
#debugging
#timer for debugging
var t

func _ready():
	rand_seed(randomSeed)
	t = Timer.new()
	t.set_wait_time(0.05)
	t.set_one_shot(true)
	self.add_child(t)
	
	# make a 2d array that save a [N,E,S,W] array for each cell
	for c in range(0,cols):
		grid.append([])
		for r in range(0,rows):
			grid[c].append([N,E,S,W])
			pass
	
	makeRooms (roomsValue , grid)
	createSpawnPoints()
	emit_signal("mazePercentDone", 25)
	#updateAll()
	while visited.has(current):
		current = Vector2(randi()%int(cols-roomSize.y*2)+roomSize.y,randi()%int(rows-roomSize.x*2)+roomSize.x)
	visited.append(current)
	
	
	var uncarved = rows*cols - visited.size()
	
	drunkardWalk(grid, uncarved*0.7)
	rBactrackMaze(grid, uncarved*0)
	mergeRoomMaze()
	updateAll()


class room:
	var position = Vector2()
	var size = Vector2()
	var Cells = []
	func setup(p,s):
		position = p
		size = s
		for i in range (0,size.x):
			for j in range (0,size.y):
				Cells.append(Vector2(position.x+i, position.y+j))
	func get_area():
		return Cells.size()

func makeRooms (noOfRooms , grid):
	#for x in range (noOfRooms):
	while rooms.size() < noOfRooms :
		#randomize()
		#pick a random room size and position
		
		var roomPosition = Vector2(floor(rand_range(0,cols-roomSize.x+1)),floor(rand_range(0,rows-roomSize.y+1)))
		# make a new room instance called randRoom and set it up with these ^ variables
		var randRoom = room.new()
		randRoom.setup(roomPosition, roomSize)
		#validation variable: checks if the room collides with other rooms
		var collides = false 
		#check if any of randRoom's cells are already in another room, if so it has collided
		for k in randRoom.Cells:
			for r in rooms:
				if r.Cells.find(k) != -1:
					collides = true; break
			if collides == true:
				break
		#if the room didnt collide, add its cells to visited and set their approriate walls in the grid
		#finally add the room instance to the rooms array
		if collides == false:
			#add this rooms cell to global roomCells Array
			for c in randRoom.Cells:
				roomCells.append(c)
			for k in randRoom.Cells:
				visited.append(k)
				#remove all walls of the cells in the grid
				grid[k.x][k.y] = []
				#add approriate walls
				if k.x == roomPosition.x:
					grid[k.x][k.y].append(W)
				if k.y == roomPosition.y:
					grid[k.x][k.y].append(N)
				if k.x == roomPosition.x + roomSize.x - 1:
					grid[k.x][k.y].append(E)
				if k.y == roomPosition.y + roomSize.y - 1:
					grid[k.x][k.y].append(S)
			#add the room to the rooms array
			rooms.append(randRoom)
			
	update()

func drunkardWalk(grid,x):
	var carved = 0
	if visited.find(current) == -1 and x>0:
		visited.append(current)
		carved += 1
	var counter = 0
	var done
	
	while carved <= ceil(x) and visited.size() < cols * rows and not done:
		#	debugging
		#t.start()
		#yield(t, "timeout")
			
		#get all neighbors
		var neighbors = checkNeighbors(current, grid, 2)
		
		var next
		while next == null or isRoomCell(next):
			next = neighbors[rand_range(0,neighbors.size())]
			neighbors.erase(next)
			
		removeWalls(next,current,grid)
		if !visited.has(next):
			visited.append(next)
			carved+=1
			counter = 0
		elif counter > (cols*rows-visited.size())*75:
#			var freshCell = getFreshCell();
#			if freshCell != null:
#				next = freshCell
#				visited.append(next)
			done = true
		else:
			counter += 1

		current = next
		#updateAll()
		

func rBactrackMaze (grid,x):
	var carved = 0
	var done
	var next
	var stack = []
	if visited.find(current) == -1 and x>0:
		visited.append(current)
		carved += 1
	while carved <= ceil(x) and visited.size() < cols * rows and not done:
		#timmer for debugging
#		t.start()
#		yield(t, "timeout")
		#end of debugging
		var neighbors = checkNeighbors(current, grid, 0)
		if neighbors.size()>0:
			next = neighbors[rand_range(0, neighbors.size())]
			removeWalls(next,current,grid)
			stack.append(current)
			visited.append(next)
			carved += 1
			current = next
		elif(stack.size() > 0):
			neighbors = checkNeighbors(current, grid, 1)
			next = null
			while !next or isRoomCell(next):
				next = neighbors[rand_range(0, neighbors.size())]
			removeWalls(next,current,grid)
			
			current = stack[-1]
			stack.pop_back()
		else:
			#current = getFreshCell();
			done = true
		if !visited.has(current):
			visited.append(current)
#		updateAll()

func getFreshCell():
	var u = 0; var searching = true
	var unvisited = findUnvisited()
	while u < unvisited.size() and searching:
		if !isRoomCell(unvisited[u]):
			searching = false;
			return unvisited[u];
		u+=1

func mergeRoomMaze():
	for room in rooms:
		current = null
		var possibleStart = room.Cells
		while !current && possibleStart.size():
			
			current = possibleStart[randi()%possibleStart.size()]
			possibleStart.erase(current)
			
			var neighbors = checkNeighbors(current,grid,2)
			
			for rc in roomCells:
				neighbors.erase(rc)
			
			if neighbors.size() > 0:
				#we have our golden boy (current is our starting cell for passage digging)
				var passage = [current]
				var next
				var done
				while !done:
					#check for posible passage end cells
					var endCells = checkNeighbors(current, grid, 1) # visited
					#check for posible passage continuation cells
					var continueCells = checkNeighbors(current, grid, 0)
					for rc in roomCells: # not a room cell
						endCells.erase(rc)
						continueCells.erase(rc)
					for p in passage: #not a passage cell
						endCells.erase(p)
					#	continueCells.erase(p)
					
					# if end cells are found
					if endCells.size()>0:
						#make the first one our end cell
						next = endCells[0]
						#add it to the passage
						passage.append(next)
						#end the digging
						done = true
					
					#otherwise if continuation cells are found
					elif continueCells.size() > 0:
						#make a random one our continue cell
						next = continueCells[randi()%continueCells.size()]
						#move to it
						current = next
						# add it to the passage array
						passage.append(current)
					#otherwise if nothing can be done
					else:
						#golden boy has failed
						current = null
						# stop digging
						break
					if passage.size() > (cols*rows):
						#golden boy has failed
						current = null
						# stop digging
						break
				# if the dig was completed successfully
				# finalize it
				if done:
					for i in range(passage.size()-1):
						#add passage cell to visited f it isnt already there
						if !visited.has(passage[i]):
							visited.append(passage[i])
						removeWalls(passage[i],passage[i+1],grid)
					
			else:
				possibleStart.erase(current)
				current = null
			

func findUnvisited():
	var unvisited = []
	for r in rows:
		for c in cols:
			var thisCell = Vector2(r,c)
			if visited.find(thisCell) == -1:
				unvisited.append(thisCell)
	return unvisited

func isRoomCell(x):
	if roomCells.find(x) == -1:
		return false
	else:
		return true

## returns all  neighbors
func checkNeighbors (vectorPos, grid, isVisited):
	var allNeighbors = [] 
	#North
	if vectorPos.y > 0 :
		allNeighbors.append(Vector2(vectorPos.x,vectorPos.y-1))
	#East
	if vectorPos.x < grid.size()-1 :
		allNeighbors.append(Vector2(vectorPos.x+1,vectorPos.y))
	#South
	if vectorPos.y < grid[vectorPos.x].size()-1 :
		allNeighbors.append(Vector2(vectorPos.x,vectorPos.y+1))
	#West
	if vectorPos.x > 0 :
		allNeighbors.append(Vector2(vectorPos.x-1,vectorPos.y))
		
	var visitedNeighbors = []
	var unvisitedNeighbors = []
		
	for n in allNeighbors : 
		if visited.find(n) != -1:
			visitedNeighbors.append(n)
		else:
			unvisitedNeighbors.append(n)
	if isVisited == 0:
		return unvisitedNeighbors
	elif isVisited == 1:
		return visitedNeighbors
	elif isVisited == 2:
		return allNeighbors

func removeWalls (a,b,grid) :
	#North
	if a.y - b.y == 1 :
		grid[a.x][a.y].erase(N); grid[b.x][b.y].erase(S)
	#East
	if a.x - b.x == -1 :
		grid[a.x][a.y].erase(E); grid[b.x][b.y].erase(W)
	#South
	if a.y - b.y == -1 :
		grid[a.x][a.y].erase(S); grid[b.x][b.y].erase(N)
	#West
	if a.x - b.x == 1 :
		grid[a.x][a.y].erase(W); grid[b.x][b.y].erase(E)
	
func drawWalls (a, grid):
	if visited.has(a):
		# NWall is true if N is not "not found" in Array2d cell a
		var NWall = grid[a.x][a.y].find(N) != -1
		var EWall = grid[a.x][a.y].find(E) != -1
		var SWall = grid[a.x][a.y].find(S) != -1
		var WWall = grid[a.x][a.y].find(W) != -1
		
		#right
		if EWall: 
			for y in range(3):
				set_cell(a.x*3+2,a.y*3+y,2);
			if !NWall && grid[a.x][a.y-1].find(E) == -1:
				set_cell(a.x*3+2,a.y*3-1,9)
			if !SWall && grid[a.x][a.y+1].find(E) == -1:
				set_cell(a.x*3+2,a.y*3+3,11)
		#left
		if WWall: 
			for y in range(3):
				set_cell(a.x*3,a.y*3+y,3);
			if !NWall && grid[a.x][a.y-1].find(W) == -1:
				set_cell(a.x*3,a.y*3-1,8)
			if !SWall && grid[a.x][a.y+1].find(W) == -1:
				set_cell(a.x*3,a.y*3+3,10)
		#top
		if NWall:
			for x in range(3):
				set_cell(a.x*3 + x ,a.y*3,0)
			if EWall:
				set_cell(a.x*3+2,a.y*3,4)
			elif grid[a.x+1][a.y].find(N) == -1:
				set_cell(a.x*3+3,a.y*3,10)
			if WWall:
				set_cell(a.x*3,a.y*3,5)
			elif grid[a.x-1][a.y].find(N) == -1:
				set_cell(a.x*3-1,a.y*3,11)
		#bottom
		if SWall:
			for x in range(3):
				set_cell(a.x*3 + x,a.y*3+2,1)
			if EWall:
				set_cell(a.x*3+2,a.y*3+2,6)
			elif grid[a.x+1][a.y].find(S) == -1:
				set_cell(a.x*3+3,a.y*3+2,8)
			if WWall:
				set_cell(a.x*3,a.y*3+2,7)
			elif grid[a.x-1][a.y].find(S) == -1:
				set_cell(a.x*3-1,a.y*3+2,9)
	else:
		for x in range (3):
			for y in range (3):
				set_cell(a.x*3+x,a.y*3+y,15);
		
	update()

func drawGround():
	var groundMap = get_node("Ground")
	var pathMap = get_node("GroundPath")
	for v in visited:
		for x in range (3):
			for y in range (3):
				if get_cell(v.x*3+x,v.y*3+y) == -1:
					pathMap.set_cell(v.x*3+x,v.y*3+y,16)

	for v in visited:
		randomize()
		var k
		var rand = randf()
		if  rand < 0.03:
			k = 0
		elif rand < 0.4 :
			k = 1
		else:
			k = 2
		groundMap.set_cell(v.x,v.y,12 + k)

func updateAll():
	for i in get_used_cells():
		set_cell(i.x,i.y,-1)
	for c in range(0,cols):
		for r in range(0,rows):
			drawWalls(Vector2(c,r),grid)
	drawGround()
	get_child(0).update()

#creater a spawn point
func createSpawnPoints():
	var n = 0
	var spawnPointScene = load("res://Scenes/SpawnPoint.tscn")
	# create a spawn point in each room
	for r in rooms:
		var point = spawnPointScene.instance()
		var mazeCellPixelSize = 3*get_cell_size()
		#set the position of the spawnPoint to the pixel center of the room
		point.set_position((r.position + r.size/2)*mazeCellPixelSize)
		#set the name of the Point
		point.set_name(str(n))
		#add the point to SpawnPoints
		SpawnPoints.add_child(point)
		n+=1

#set the maze color modulate:
func set_maze_color(color):
	pass