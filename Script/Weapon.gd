extends Node2D
export var accuracy = 0.8
export var fireRate = 10
export(PackedScene) var bulletScene 
export var staggerDistance = 64
var bulletColor = Color(1,1,1,1)
export var MAX_AMMO = 50
sync var ammo
export var damage = 1
onready var character = get_parent().get_parent()

enum { rifle = 0, melee = 3}
export var type = 0

signal ammoChanged(x)

func _ready():
	ammo = MAX_AMMO
	get_node("Timer").wait_time = 1.0/fireRate

func _process(delta):
	if character.weapons[character.currentWeaponId] == self:
		adjustZ_index()
			

func adjustZ_index():
	if character.lookDir.y > 0:
		z_index = 0
	else:
		z_index = -1
	
func shoot(lookDir):
	if ammo > 0:
		if get_node("Timer").is_stopped() and lookDir != Vector2(0,0):
			ammoChangeBy(-1)
			var bullet = bulletScene.instance()
			bullet.color = bulletColor
			bullet.shooter = character
			bullet.damage = damage
			var currentAngle = -atan2(lookDir.x,lookDir.y)
			var newAngle = currentAngle + (randf()-0.5)*(PI/2)*(1-accuracy)
			lookDir = Vector2(-sin(newAngle),cos(newAngle))
			rotation = newAngle
			bullet.staggerDistance = staggerDistance
			bullet.motion = lookDir

			bullet.position = get_node("bulletSpawn").global_position - character.get_parent().global_position
			bullet.canTeleport = true
			character.get_parent().add_child(bullet)
			
			get_node("Timer").start()
		character.shakeCamera()

func ammoChangeBy(x):
	if ammo + x > MAX_AMMO:
		ammo = MAX_AMMO
	else:
		ammo += x
	emit_signal("ammoChanged", ammo, MAX_AMMO )


func update(direction):
	rotation = -atan2(direction.x,direction.y)