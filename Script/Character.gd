extends KinematicBody2D
# Constants
var MOTION_SPEED = 750 # Pixels/second
var glide = 0.7
var finalSpeed

var characterColor = Color(1,1,1)
var charName

var moveDir = Vector2(0,0)
var velocity = Vector2()
var lookDir = Vector2()

slave var slavePos = Vector2()
slave var slaveVelocity = Vector2()
slave var slaveLookDir = Vector2()

#weapons
var weapons = []
var currentWeaponId=0 

#required by portaling
export var canTeleport = true
var portalWait = 0.25
onready var portalTimer = get_node("portalTimer")
onready var health = get_node("Health")
onready var energy = get_node("Energy")

#required for death and drops:
sync var hasDropped = false

#required for slave network sync to master
var UPDATE_INTERVAL = 0.02 #33 Hz packet sending frequency 
var lastSlavePos
var lastSlaveVelocity
var netTime = 0
var lastPacketTime = 0
var packetTime = 0
var lerpTime = 0
var lerpVelocity = Vector2(0,0)
var packetCount = 0

#stagger
var staggering = false
var staggerVel = Vector2(0,0)

enum {
	SPR_UP = 1, SPR_DOWN = 2, SPR_LEFT = 4, SPR_RIGHT = 8,
} 

# "Private" members
onready var sprite_direction = { # All 8 directions for a sprite's animation
	SPR_UP					: "walkUp"		,
	SPR_DOWN				: "walkDown"	,
	SPR_LEFT				: "walkLeft"	,
	SPR_RIGHT				: "walkRight"	,
	SPR_UP   + SPR_LEFT		: "walkLeft"	,
	SPR_UP   + SPR_RIGHT	: "walkRight"	,
	SPR_DOWN + SPR_LEFT		: "walkLeft"	,
	SPR_DOWN + SPR_RIGHT	: "walkRight"	,
}
onready var sprite_velocity = {
	SPR_UP					: Vector2( 0, -1),
	SPR_DOWN				: Vector2( 0,  1),
	SPR_LEFT				: Vector2(-1,  0),
	SPR_RIGHT				: Vector2( 1,  0),
	SPR_UP   + SPR_LEFT		: Vector2(-1, -1),
	SPR_UP   + SPR_RIGHT	: Vector2( 1, -1),
	SPR_DOWN + SPR_LEFT		: Vector2(-1,  1),
	SPR_DOWN + SPR_RIGHT	: Vector2( 1,  1),
}

signal mapChanged(newMapIndex) 

func _ready():
	for w in get_node("Weapons").get_children():
		weapons.append(w)
	switchWeapon(0)
	if !portalTimer.is_connected("timeout", self, "disablePortalProg"):
		portalTimer.connect("timeout", self, "disablePortalProg")
	disablePortalProg()
	health.connect("dead",self,"killed",[])

func think(delta):
	# code to get velocity direction goes here. it must return a vector
	moveDir = Vector2(0,0)
	#code related to the looking direction of the charcter goes here, it must update the variable "lookDir"
	lookDir = (get_global_mouse_pos() -  get_global_pos()).normalized()

func _process(delta):
	netTime += delta
	if is_network_master():
		think(delta)
#		#update slave
		if canTeleport and netTime > UPDATE_INTERVAL:
			rset("slavePos", position)
			rset("slaveVelocity", velocity)
			rset("slaveLookDir", lookDir)
			netTime = 0
	else:
		netTime += delta

		if lastSlavePos != slavePos || lastSlaveVelocity != slaveVelocity: #When we recieve a packet
			#print("------------------------------------------")

			#update the player position every half a second)
			packetCount += 1
			if !(packetCount % 30):
				position = slavePos 

			packetTime = netTime #save the time this packet was recieved

			var elapsed = packetTime - lastPacketTime #calculated elapsed time between this and the last packet

			#this elapsed time is an estimate of how long it took this packet to get here

			#since the time the packet (slavePos) was sent, it must have changed
			var extrapSlavePos = slavePos + slaveVelocity * elapsed  #extrapolate the new slave position
			#print(str(slavePos) + " ---> " + str(extrapSlavePos))

			var jitter = elapsed - UPDATE_INTERVAL    # did current packet take longer?
			#print("jitter = " + str(jitter))
			# lerp for more time than the update_interval
			lerpTime = UPDATE_INTERVAL + jitter    #since the next update will probably also come later

			#print("lerpTime = " + str(lerpTime))

			lerpVelocity = (extrapSlavePos-position)/lerpTime
			#print("lerpVelocity = " + str(lerpVelocity))

			lastPacketTime = packetTime #the packetTime (time this packet was recieved) becomes lastPacketTime
			lastSlavePos = slavePos #this position becomes the lastSlavePos
			lastSlaveVelocity = slaveVelocity 

			velocity = lerpVelocity
		lookDir = slaveLookDir

	if !portalTimer.is_stopped():
		var x =  100 - portalTimer.get_time_left()*100/portalTimer.get_wait_time()
		get_node("portalProg").set_value(x)


func _physics_process(delta):
	if is_network_master():
	#	if moveDir == Vector2(0,0):
	#		velocity *= glide
	#	else:
		velocity = moveDir.normalized() * MOTION_SPEED

		move_and_slide(velocity)
	elif lerpTime > 0:
		move_and_slide(lerpVelocity)
		lerpTime -= delta
	
	if staggering:
		move_and_slide(staggerVel)
	weapons[currentWeaponId].update(lookDir)
#

###############
### Methods ###
###############
#slave func animate_character(directions):
	# If it has animation in this direction
#	if sprite_direction.has(directions):
#		var new_anim = sprite_direction[directions]
#		if new_anim != Anims.get_current_animation() || !Anims.is_playing():
#			Anims.play(new_anim)
#	elif Anims.is_playing():
#		Anims.stop()
#	return Anims.is_playing()
 
func stagger(velocity,time):
	staggering = true
	staggerVel = velocity
	var t = Timer.new()
	t.wait_time = time
	t.one_shot = true
	add_child(t)
	t.start()
	yield(t,"timeout")
	staggering = false
	t.queue_free()

sync func shoot(direction):
	weapons[currentWeaponId].shoot(direction)

func set_character_name(x):
	get_node("label").set_text(x)
	charName = x

func get_character_name():
	return get_node("label").get_text()

func set_character_color(color):
	characterColor = color
	get_node("Sprite").set_modulate(color)

func portalCountDown():
	portalTimer.set_wait_time(portalWait)
	portalTimer.set_one_shot(true)
	portalTimer.start()
	get_node("portalProg").show()

func disablePortalProg():
	portalTimer.stop()
	get_node("portalProg").hide()

func set_health(x):
	health.health = x

func killed(shooter):
	var randSeed = randi()
	if is_network_master():
		rpc("dropItem",randSeed)
		if is_in_group("player"):
			gamestate.playerState = gamestate.DEAD
			
	var t = Timer.new()
	t.wait_time = 0.1
	t.one_shot = true
	add_child(t)
	while !hasDropped:
		t.start()
		yield(t,"timeout")
	t.queue_free()
	
	if is_inside_tree():
		get_parent().remove_child(self)
	
	return true;

sync func dropItem(randSeed):
	hasDropped = true

func shakeCamera():
	var camera = get_node("camera")
	var t = Timer.new()
	t.wait_time = 0.2
	t.one_shot = true
	for i in range(3):
		camera.offset = Vector2(randf()-0.5,randf()-0.5) * 6
		t.start()
		yield(t,"timeout")
	camera.offset = Vector2(0,0)
	t.queue_free()

func switchWeapon(id):
	currentWeaponId = id
	for w in weapons:
		w.visible = false
	weapons[id].visible = true
	
