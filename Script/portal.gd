extends Area2D
var size = Vector2(0,0)
var wall = 0
var destination = null
var color = Color(1,1,1)
var active = true

func _ready():
	var wallPos = size
	var points 
	if wall == 0: #North
		points = [Vector2(size.x,0),Vector2(size.x * 2,0),Vector2(size.x *2,size.y),size]
	elif wall == 1: #East
		points = [Vector2(size.x*3,size.y),Vector2(2*size.x,size.y),size*2,Vector2(size.x*3,size.y*2)]
	elif wall == 2: #South
		points = [Vector2(size.x,size.y*2),size*2,Vector2(2*size.x,3*size.y),Vector2(size.x,3*size.y)]
	elif wall == 3: #West
		points = [Vector2(0,size.y),Vector2(0,size.y*2),Vector2(size.x,size.y*2),size]
		
	var polygon = ConvexPolygonShape2D.new()
	polygon.set_points(points)
	
	var collision = CollisionShape2D.new()
	collision.set_shape(polygon)
	add_child(collision)
	
	#get_child(0).set_polygon(points)
	#get_child(0).set_color(color)
	
	if active:
		connect("body_entered",self,"_body_enter_portal", [destination])
	connect("body_exited",self,"_body_exit_portal")

func _body_enter_portal(body, destination):
	if body != null:
		if body.get("canTeleport") == true:
			if is_network_master():
				if body.is_in_group("character"):
					body.portalCountDown()
					body.portalTimer.connect("timeout", self, "rpc", ["_teleportToNode", body.get_path(), destination.get_path()])
				else:
					rpc("_teleportToNode", body.get_path(), destination.get_path())
			elif body.is_in_group("character"):
				body.portalCountDown()

sync func _teleportToNode(bodyPath, destinationPath):
	var body = get_node(bodyPath)
	var destination = get_node(destinationPath)
	if body != null:
		#body.set_collision_mask(destination.get_collision_layer())
		#body.set_collision_layer(destination.get_collision_layer())
		if body.is_in_group("character") and body.portalTimer.is_connected("timeout", self, "rpc"):
			body.portalTimer.disconnect("timeout", self, "rpc")
			body.velocity *= -1
		body.canTeleport = false
		
		if body.is_in_group("player") and (body.is_network_master() or gamestate.playerState == gamestate.DEAD):
#			destination.set_visible(true)
			if body.is_network_master():
				body.emit_signal("mapChanged", destination.get_index())
		
		if body.is_in_group("bullet"):
			body.linear_velocity *= -1
			body.rotation = -atan2(body.motion.x,body.motion.y) + PI/2
			#body.set_collision_mask(destination.get_collision_layer())
			#body.set_collision_layer(destination.get_collision_layer())
		
		disconnect("body_exited",self,"_body_exit_portal")
		body.get_parent().remove_child(body)
		connect("body_exited",self,"_body_exit_portal")
		
		destination.add_child(body)
		
		
func _body_exit_portal(body):
	if body.is_in_group("character"):
		body.disablePortalProg()
		if body.portalTimer.is_connected("timeout", self, "rpc"):
			body.portalTimer.disconnect("timeout", self, "rpc")
	if body.get("canTeleport") != null:
		body.canTeleport = true
	
