extends RigidBody2D
var itemID = 0;
var itemDrop = droptable.items[itemID];
var message; var area

func _ready():
	itemID = itemDrop.id
	var image = load(itemDrop.path)
	get_node("Sprite").texture = image
	get_node("Sprite").self_modulate = itemDrop.color
	get_node("Sprite").update()
	
	message = get_node("HUDMessage")
	
	message.get_node("Sprite").texture = image
	message.get_node("Sprite").modulate = itemDrop.color

	area = get_node("Area2D")
	#area.set_collision_layer(get_collision_layer())
	#area.set_collision_mask(get_collision_mask())

func dropEffect(player):
	if itemDrop.dropEffect.call_func(player):
		message.get_node("Label").text = str(itemDrop.amount) + " " + itemDrop.name
		return true
	

func _on_Area2D_body_entered(body):
	if body.is_in_group("player"):
		if body.is_network_master() and dropEffect(body):
			message.visible = true
			remove_child(message)
			gamestate.HUD.pickedDrop(message)
			queue_free()