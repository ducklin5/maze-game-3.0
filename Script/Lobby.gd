extends Control
var config = ConfigFile.new()
var udp = PacketPeerUDP.new()
var PORT = gamestate.PORT
const MAGIC = "sfjs343"
var hostsArray = []
onready var yourIP = find_node("yourIP")
onready var hostList = get_node("connect/Tabs/Join/hostList")
onready var joinIP = get_node("connect/Tabs/Join/joinIP")
onready var hostPortNode = get_node("connect/Tabs/Host/port")
onready var joinPortNode = get_node("connect/Tabs/Join/port")

var my_ip
var broadcastAddr = ""
var t = Timer.new()

func _ready():
	#get settings from file
	var err = config.load("user://settings.cfg")
	if err != OK or config.get_value("Player","color") == null:
		config.set_value("Player","name", "Player"+str(randi()))
		config.set_value("Player","color", Color(1,1,1))
		config.save("user://settings.cfg")
	else:
		get_node("connect/Tabs/Settings/nick").set_text(config.get_value("Player","name"))
		get_node("connect/Tabs/Settings/colorPick").set_pick_color(config.get_value("Player","color"))

	# get local address
	var shortestLength = IP.get_local_addresses()[0].length()
	for my_ip in IP.get_local_addresses():
		yourIP.add_item(my_ip)
		if my_ip.length() <= shortestLength:
			joinIP.text = my_ip
	
	# set the local address as the default join address
	

	#connect signals
	gamestate.connect("player_list_changed", self, "refresh_lobby")

	#get game servers on local network
	#hostList.set_fixed_column_width(10)
	refresh_hostList()

#function for the host button
func _on_hostButton_pressed():
	PORT = hostPortNode.value
	# if the nick name field in empty
	if (find_node("nick").get_text() == ""):
		invalidNick()
		return;
	udp.close()

	#var my_ArrayIP = my_ip.split(".")
	#for i in range(3):
	#	broadcastAddr += (my_ArrayIP[i] + ".")
	broadcastAddr = "255.255.255.255"

	#create timer
	t.wait_time = 3
	t.one_shot = true
	add_child(t)
	t.start()
	var err = udp.set_dest_address(broadcastAddr,PORT)
	if (err != OK):
		print("Error:\nCan't resolve.")
	else:
		var namevar = find_node("nick").get_text()
		var colorvar =  find_node("colorPick").get_pick_color()
		var playerInfo = {"name":namevar, "color":colorvar}
		gamestate.host_game(playerInfo, find_node("maxPlayers").get_value(), PORT)
		showWaitLobby()
		refresh_lobby()
		#brocast the game information
		var data = [MAGIC,get_node("connect/Tabs/Host/serverName").get_text()]
		var dataErr
		while true:
			dataErr = udp.put_var(data)
			#print(err) debugging
			yield(t,"timeout")
			t.start()
			
func refresh_hostList():
	PORT = joinPortNode.value
	if !udp.is_listening():
		var error = udp.listen(PORT)
		if (error != OK):
			print("Error:\nCan't listen.")
	if udp.is_listening():
		get_node("connect/Tabs/Join/refresh").set_disabled(true)
		hostsArray = []
		hostList.clear()
		var n = 10; var packet; var hostIP; var port;
		while n > 0:
			if udp.get_available_packet_count() > 0:
				packet = udp.get_var()
				hostIP = udp.get_packet_ip()
				port = udp.get_packet_port()
				if typeof(packet) == TYPE_ARRAY and  packet[0] == MAGIC and !hostsArray.has(hostIP):
					hostsArray.append(hostIP)
					hostList.add_item(packet[1])
			yield(t,"timeout")
			n-=1
		get_node("connect/Tabs/Join/refresh").set_disabled(false)


	

func _on_hostList_item_selected( index ):
	var ip = hostsArray[index]
	joinIP.text = ip
	update()

func _on_joinButton_pressed():
	if (find_node("nick").get_text() == ""):
		invalidNick()
	else:
		PORT = joinPortNode.value
		var ip = IP.resolve_hostname(joinIP.text,IP.TYPE_IPV4)
		var namevar = find_node("nick").get_text()
		var colorvar =  find_node("colorPick").get_pick_color()
		var playerInfo = {"name":namevar, "color":colorvar}
		gamestate.join_game(playerInfo, ip, PORT)
		showWaitLobby()

func invalidNick():
	find_node("nickError").show()
	find_node("Tabs").set_current_tab(2)
func showWaitLobby():
	find_node("nickError").hide()
	get_node("connect").hide()
	get_node("wait").show()

func refresh_lobby():
	var players = gamestate.players
	get_node("wait/playerList").clear()
	for id in players:
		if id == get_tree().get_network_unique_id():
			get_node("wait/playerList").add_item( "-> " + players[id].name + " (You)")
		else:
			get_node("wait/playerList").add_item(players[id].name)
	get_node("wait/playerList").sort_items_by_text()
	get_node("wait/start").set_disabled(not get_tree().is_network_server())

func _on_start_pressed():
	gamestate.begin_game()

func _on_nick_text_changed( text ):
	config.set_value("Player","name", text)
	config.save("user://settings.cfg")

func _on_colorPick_color_changed( color ):
	config.set_value("Player","color", color)
	config.save("user://settings.cfg")
