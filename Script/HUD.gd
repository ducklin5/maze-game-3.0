extends CanvasLayer
var healthBar; var deathNotice; var respawnButton; var energyBar; var mapIndicator;
var bossProg;
#var pos = Vector2( 
enum {UNKOWN = 0, INACTIVE = 1,  ACTIVE = 2}
var mapState = []

signal respawn

func _ready():
	healthBar = get_node("healthBar")
	energyBar = get_node("energyBar")
	deathNotice = get_node("deathNotice")
	respawnButton = get_node("respawnButton")
	mapIndicator = get_node("mapIndicator")
	bossProg = get_node("BossProg")

func _on_energyChanged(energy,maxEnergy):
	energyBar.set_max(maxEnergy)
	energyBar.set_value(energy)
	energyBar.update()
	

func _on_healthChanged(health,maxHealth):
	healthBar.set_max(maxHealth)
	healthBar.set_value(health)
	healthBar.update()

func _player_died(shooter):
	deathNotice.visible = true
	respawnButton.visible = true


func _on_respawnButton_pressed():
	emit_signal("respawn")

func _on_mapChanged(currentMapIndex):
	print("map Changed")
	for i in mapState.size():
		if mapState[i] == ACTIVE: mapState[i] = INACTIVE
	mapState[currentMapIndex] = ACTIVE
	updateMapIndicator()

func updateMapIndicator():
	if mapState.size() != mapIndicator.get_child_count():
		for i in mapIndicator.get_children():
			i.queue_free()
		var indicatorScene = load("res://Scenes/indicator.tscn")
		var i = 0
		for state in mapState:
			var indicator =  indicatorScene.instance()
			indicator.position = Vector2(40 * i, 0)
			mapIndicator.add_child(indicator)
			i += 1
	for mapIndex in mapState.size():
		print(mapState)
		match mapState[mapIndex]:
			UNKOWN:
				mapIndicator.get_child(mapIndex).modulate = Color(1,1,0,1)
			INACTIVE:
				mapIndicator.get_child(mapIndex).modulate = Color(0,0,0)
			ACTIVE:
				mapIndicator.get_child(mapIndex).modulate = Color(1,1,1)
	mapIndicator.update()

func pickedDrop(message):
	get_node("drops").add_child(message)
	var t = Timer.new()
	t.wait_time = 3
	t.one_shot = true
	get_node("drops").add_child(t)
	t.start()
	yield(t,"timeout")
	message.queue_free()

func ammoChanged(ammo,ammoMax):
	var ammoLabel = get_node("WeaponStats/Ammo")
	ammoLabel.text = str(ammo) + "/" + str(ammoMax)
	ammoLabel.update()

func switchWeapon(weapon):
	if get_node("WeaponStats").get_child_count() >= 3:
		get_node("WeaponStats").get_child(2).queue_free()
	
	get_node("WeaponStats").get_node("Name").text = weapon.name
	
	var weaponSprite = weapon.find_node("Sprite").duplicate()
	weaponSprite.rotation = PI/4
	weaponSprite.offset = Vector2(0,0)
	weaponSprite.position = Vector2(32,0)
	get_node("WeaponStats").add_child(weaponSprite)

func updateBossProg(currentVal,maxVal):
	bossProg.max_value = maxVal
	bossProg.value = currentVal
	print ("bossProg.max_value = " +str(maxVal)+ " " + "bossProg.value = "+ str(currentVal))
	bossProg.update()
	bossProg.show()
	bossProg.get_node("visibilityTimer").start()
	yield(bossProg.get_node("visibilityTimer"),"timeout")
	bossProg.hide()
	print("invis mode >>>>>>>>>>>>>>>>>>>>>>>>>>>>")
	bossProg.update()