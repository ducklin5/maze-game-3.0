extends "res://Script/Character.gd"

onready var camera = get_node("camera")
onready var sprite = get_node("Sprite")
var p_id
var directions
var idleAccum = 0
var Speed = 100
var Strength = 100
var Duration = 100
var activeAbilities = [0,0,0,0]
var abilityDrain = [5, 20,20,20]
var canMove = true

func _ready():
	if !is_network_master():
		get_node("lootGravity").space_override = 0
		set_process_unhandled_input(false)
	weapons[currentWeaponId].bulletColor = characterColor

	

func _unhandled_input(event):
	if event.is_action("ui_shift") and canMove:
		if event.is_action_pressed("ui_shift"):
			MOTION_SPEED = 7.5*Speed
		if event.is_action_released("ui_shift"):
			MOTION_SPEED = 5*Speed
	if event.is_action_pressed("ability1"):
		if !activeAbilities[0] and energy.value >= abilityDrain[0] :
			rpc("ability1");
		elif energy.value < abilityDrain[0]:
			print("not enough energy")
		else:
			print("ability is active")
	if event.is_action_pressed("nextWeapon"):
		if currentWeaponId == weapons.size() - 1:
			rpc("switchWeapon", 0)
		else:
			rpc("switchWeapon", currentWeaponId+1)
			

func think(delta):
#	var newIdleAccum = idleAccum + delta
#	while newIdleAccum > 1.0/60.0:
#		newIdleAccum -= 1.0/60.0
#	var diff = idleAccum - newIdleAccum
#	idleAccum = newIdleAccum
#	if diff>0.01 : print("SKIP!", diff)
	
	directions = int(Input.is_action_pressed("ui_up"))  * 1
	directions	+= int(Input.is_action_pressed("ui_down"))  * 2
	directions	+= int(Input.is_action_pressed("ui_left"))  * 4
	directions	+= int(Input.is_action_pressed("ui_right")) * 8
	if !activeAbilities[0]:
		if sprite_velocity.has(directions):
			moveDir = sprite_velocity[directions]
		else:
			moveDir = Vector2(0,0)
			
		if Input.is_mouse_button_pressed(1):
			shoot(lookDir)
			rpc("shoot", lookDir)
			
	
	lookDir = (get_global_mouse_position() -  get_global_position()).normalized()

sync func ability1():
	canMove = false
	energy.changeBy(-abilityDrain[0])
	activeAbilities[0] = true
	get_node("Trail").emitting = true
	var t = Timer.new()
	t.wait_time = 0.5
	t.one_shot = true
	add_child(t)
	t.start()
	moveDir = lookDir
	MOTION_SPEED = Speed * 13
	yield(t,"timeout")
	get_node("Trail").emitting = false
	activeAbilities[0] = false
	t.queue_free()
	MOTION_SPEED = 5*Speed
	canMove = true

#func ability2():
#	energy.changeBy(-abilityDrain[0])
#	activeAbilities[0] = true
#	var t = Timer.new()
#	t.wait_time = 10
#	t.one_shot = true
#	add_child(t)
#	t.start()
#	moveDir = Vector2(-1,0)
#	Speed = Speed * 3
#	print("active")
#	yield(t,"timeout")
#	print("deactivated")
#	t.queue_free()
#	Speed = Speed/3
#	activeAbilities[0] = false

sync func switchWeapon(id):
	print("playerWeaponSwitch")
	if is_network_master() :
		if weapons[currentWeaponId].is_connected("ammoChanged",gamestate.HUD,"ammoChanged"):
			weapons[currentWeaponId].disconnect("ammoChanged",gamestate.HUD,"ammoChanged")
		weapons[id].connect("ammoChanged",gamestate.HUD,"ammoChanged")
		weapons[id].emit_signal("ammoChanged", weapons[id].ammo, weapons[id].MAX_AMMO)
		gamestate.HUD.switchWeapon(weapons[id])
	.switchWeapon(id)