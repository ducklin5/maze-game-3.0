extends Node2D
export(float) var attackSpeed = 1
export var staggerDistance = 64
export var damage = 100
onready var holder = get_parent().get_parent()
onready var weaponBody = get_node("pivot/weaponBody")
onready var sprite = get_node("pivot/weaponBody/Sprite")
export var MAX_AMMO = 500
sync var ammo
enum {ATTACK, IDLE}
var state = IDLE
enum {CLOCKWISE, ANTICLOCKWISE}
var nextSwing = ANTICLOCKWISE
var motion=Vector2(1,0)
export var speed = 1400
var prev_pos
var prev_rot


signal ammoChanged(x)

func _ready():
	set_process(true)
	
	get_node("AnimationPlayer").playback_speed = attackSpeed
	ammo = MAX_AMMO
	prev_pos = sprite.global_position
	

func _process(delta):
	if holder.weapons[holder.currentWeaponId] == self:
		if holder.lookDir.x > 0:
			z_index = 0
		else:
			z_index = -1
	

	
func shoot(lookDir):
	if state == IDLE and lookDir != Vector2(0,0):
		changeState(ATTACK)

func changeState(newState):
	match newState:
		ATTACK:
			match nextSwing: 
				CLOCKWISE:
					get_node("AnimationPlayer").play("ClockAttack")
				ANTICLOCKWISE:
					get_node("AnimationPlayer").play("AntiClockAttack")
			state = ATTACK
		IDLE:
			state = IDLE

func ammoChangeBy(x):
	pass


func update(direction):
	#if state == IDLE:
	rotation = -atan2(direction.x,direction.y)
	

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "AntiClockAttack":
		changeState(IDLE)
		nextSwing = CLOCKWISE
	elif anim_name == "ClockAttack":
		changeState(IDLE)
		nextSwing = ANTICLOCKWISE

func _on_weaponBody_body_entered(body):
	var bodygroups = body.get_groups()
	var holdergroups = holder.get_groups()
	if body != holder and body != self:
		if body.is_in_group("character"):
			body.stagger(motion * speed, staggerDistance /(motion * speed).length())
			if body.is_network_master():
				body.health.rpc("hit",damage, holder)
		if body.is_in_group("bullet"):
			body.queue_free()
