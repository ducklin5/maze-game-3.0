extends RigidBody2D

var shooter 
var canTeleport = true
var motion=Vector2(1,0)
export var speed = 1400
var color = Color(1,0,0)
export var damage = 10
var staggerDistance = 64

var hit=false
onready var sprite = get_node("Sprite")

func _ready():
	set_physics_process(true)
	get_node("Light2D").color = color
	get_node("animePlayer").play("sprite")
	linear_velocity = (motion*speed)
	bounce = 0
	linear_damp = 0.0
	sleeping = false
	contact_monitor = true 
	contacts_reported = 1
	rotation = -atan2(motion.x,motion.y) + PI/2

func _on_Bullet_body_entered(body):
	var bodygroups = body.get_groups()
	var shootergroups = shooter.get_groups()
	if body != shooter and body != self:
		if body.is_in_group("character"):
			body.stagger(motion * speed, staggerDistance /(motion * speed).length())
			if body.is_network_master():
				body.health.rpc("hit",damage, shooter)
			
	queue_free()