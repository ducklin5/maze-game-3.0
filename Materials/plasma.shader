shader_type canvas_item;
uniform float rectX;
uniform float rectY;
uniform float rectW;
uniform float rectH;
uniform sampler2D maskTexture; 
uniform vec4 plasmaColor;
void fragment() {
	//needed constant
	float PI = 3.1415926535897932384626433832795;
	float time = TIME/8.0;
	
	//calculate the UV of the picture relative to its region
	vec4 rect = vec4(rectX, rectY, rectH, rectW);
	vec2 fRectPos = rect.xy / vec2(ivec2(textureSize(TEXTURE,0)));
	vec2 fRectSize = rect.zw / vec2(ivec2(textureSize(TEXTURE,0)));
	vec2 rectUV = (UV.xy-fRectPos)/fRectSize + POINT_COORD.xy;

	//The actuall shader code
	vec2 scale = vec2(1.0,1.0) * 0.5;
	vec2 newCoords =  rectUV * scale;
	
	float v1x = sin((newCoords.x + time) *  2.0  * PI );
	float v1y = sin((newCoords.y + time) *  2.0  * PI );
	float v2 = sin(dot((newCoords-vec2(0.5,0.5))*10.0 , vec2(sin(time/2.0),cos(time/3.0))));
	
	vec2  c = newCoords - vec2(0.5,0.5) + vec2(sin(time/3.0), cos(time/2.0)); 
	float v3 = sin(
		sqrt(
			125.0*(c.x *c.x + c.y * c.y) + 1.0
		)
		+ time*3.0
	);
	
	float v = v1x+v1y+v2+v3;
	vec4 original_color = texture(TEXTURE, UV);
	vec4 maskVal = texture(maskTexture,UV);
	vec4 shaderColor = mix(vec4(vec3(sin(v*PI)), 1),plasmaColor,0.5);
	vec4 mixedColor =  original_color * ((shaderColor-vec4(1))*maskVal+vec4(1)) ;
	//mixedColor = mix(original_color,shaderColor,maskVal.x);
	
	COLOR = mixedColor;

}
